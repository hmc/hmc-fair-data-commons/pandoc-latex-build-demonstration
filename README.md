# Use Gitlab CI to build markdown documents with pandoc-latex-build

## Instructions

### Clone or fork this repository

You can replace the default `latex-template` with one you prefer.

**Alternatively**, set up a pandoc-latex-build project
(for this you need
[pandoc-latex-build](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build)
installed on your computer)
and add the `.gitlab-ci.yml` file provided in this repository.

**Optional, if you forked this repository:**

To work around
[a limitation in Gitlab](https://gitlab.com/gitlab-org/gitlab/-/issues/15598),
you might want to remove the fork status of your copy, otherwise you won't be
able to use the **Create Merge Request** button from an issue. If you need this
feature, go to **General -> Advanced -> Remove Fork Relationship** and confirm
it by entering the name of your repository.

### Set up the badges

**NOTE**: You need at least **maintainer** permissions to your repository to
perform this step.

**This is mostly optional, but for convenience you should add at least the
badge linking to the PDF output or the overview page for all formats**.

#### In your repository, go to **Settings -> General -> Badges** (and **expand** the section if it is collapsed).

##### To add "pipeline status" badge, fill out the fields as follows and then press **Add badge**:

**Name:** Pipeline Status

**Link:** `https://codebase.helmholtz.cloud/%{project_path}/-/commits/%{default_branch}`

**Badge image URL:** `https://codebase.helmholtz.cloud/%{project_path}/badges/%{default_branch}/pipeline.svg`

##### To add a "document X" badge (where **X** can be one of **pdf**, **odt**, **html** or **icml**):

**Name:** document.**X**

**Link:** `https://codebase.helmholtz.cloud/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/public/document.`**X**`?job=pages`

**Badge image URL:** `https://codebase.helmholtz.cloud/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/document_`**X**`.svg?job=pages`

##### To also add a "show all formats" badge, use the following values for the fields:

**Name:** Link to Pages

**Link:** If your repository is found under
`https://codebase.helmholtz.cloud/hmc/a/b/c`, use
`https://hmc.pages.hzdr.de/a/b/c` as link

**Badge image URL:** `https://codebase.helmholtz.cloud/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/pages.svg?job=pages`

### Final preparations

Now you might want to

* remove the example content in `source` (if you do not use git locally,
  e.g. use the Web IDE, where you can also delete files)

* restrict direct pushes to the main branch (in **Settings -> Repository ->
  Protected Branches** restrict **Allowed to push** to **Maintainers** or
  **no one**)

### Start working on your document

From now on, you should only ever need to touch the files located in `source`
and the `CITATION.cff` (for updating the authors and affiliations).

## Usage

Once you have set up the `.gitlab-ci.yml` file and push a commit that includes
the file, the runner should start automatically and be executed on every new
commit to build the document.

The resulting document should be available as [Gitlab Pages](../../../pages).

But of course the documents are available as
[job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#access-the-latest-job-artifacts-by-url)
as well and you can link them using Badges
(['Settings' -> 'General'](../../../edit) expand 'Badges').

Further you can use [pre-commit](https://pre-commit.com/) in
continuous integration to check your source code files (markdown, tex, ...).
Look at the provided [pre-commit hooks](https://pre-commit.com/hooks.html) to
see possible tools.

This is already done as an example in [.gitlab-ci.yml](.gitlab-ci.yml) in this
repository. The jobs are running in the same stage in parallel. But
you could also run first the check and then the deployment of the pages
in sequence.
Since the markdown files in this repository do not fulfill these requirements,
the CI job is allowed to fail. You can see the errors/hints as an example.
Since pandoc is still able to handle the files, we can go with them.

## Citation File Format

This example uses the
[Citation File Format](https://github.com/citation-file-format/citation-file-format).
But to provide the same features as the default/example template from
[pandoc-latex-build](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build)
we use here some additional fields. If you do not need them,
you can safely remove these fields.

### additional fields for/in pandoc

There are some additional fields in the global hierarchy which are not
defined/allowed in [cff-version: 1.2.0](https://doi.org/10.5281/zenodo.5171937):

* `subtitle`

For the `authors` field we have also some additional fields which are not
defined/allowed in [cff-version: 1.2.0](https://doi.org/10.5281/zenodo.5171937):

* `notices`

### additional fields for/in CITATION.cff

The [cff-version: 1.2.0](https://doi.org/10.5281/zenodo.5171937) requires an
additional global field which we do not use here in the template:

* `message`
